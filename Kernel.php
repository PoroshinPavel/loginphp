<?php

class Kernel{
    protected $data = [];
    
    protected function createUser($data){
        $this->createFile($this->getId($data), $data);
    }



    protected function createFile($id, $data){
        $name = $data[0];
        $surname = $data[1];
        $email = $data[2];
        $number = $data[3];
        $confer = $data[4];
        $pay = $data[5];
        $compare = $this->compareFiles($data);
        if($compare == True || $compare=='nothing'){
            $user = fopen('./db/' . $id . '_' . $name . '_' . $surname . '_' . $number. '.txt' , 'w+');
            fwrite($user, $id . '\n' . $name . '\n' . $surname . '\n' . $email . '\n' . $number . '\n' . $confer . '\n' . $pay);
            fclose($user);
            echo "<link rel='stylesheet' href='main.css'>Пользователь успешно зарегистрирован";
        }
        else{
            echo "<link rel='stylesheet' href='main.css'>Пользователь с таким номером уже зарегистрирован";
        }
    }




    protected function getListOfFiles(){
        $files = glob('db/*.txt');
        return $files;
    }





    private function getId(){
        $identifires = [];
        $current_files = $this->getListOfFiles();
        if($current_files == False){
            return 0;
        }
        else{
            foreach($current_files as $file){
                $identifires[] = intval(explode('_', (explode('/', $file)[1]))[0]);
            }
        }
        return max($identifires)+1;
    }




    protected function compareFiles($data){
        $current_files = $this->getListOfFiles();
        $current_number = $data[3];
        if ($current_files){
            foreach($current_files as $file_name){
                $number_in_file = explode('_', explode('.', $file_name)[0])[3];
                if($number_in_file == $current_number){
                    return False;
                }
            }
            return True;
        }
        else{
            return 'nothing';
        }
    }
}

class Admin extends Kernel{

    public function getUsersForDelite($list){
        $this->deliteUsers($list);

    }
    public function showListOfUsers(){
        $this->getListOfUsers();
    }

    private function parseFileName($file_name){
        $parse1 = explode('/', $file_name)[1]; 	  //without '/db'
        $parse2 = explode( '.', $parse1)[0]; //without '.txt'
        $user_data = explode('_', $parse2);
        return $user_data;
    }

	protected function getListOfUsers(){
    	$files = $this->getListOfFiles();
    	foreach($files as $file){
            $user_data_ = $this->parseFileName($file);
            $ID = strval($user_data_[0]);
            $NAME = strval($user_data_[1]);
            $SURNAME = strval($user_data_[2]);
            $NUMBER =  strval($user_data_[3]);
            $forValue = $ID . ' - ' . $NAME . ' ' . $SURNAME . ' ' . $NUMBER ;
        	echo "<input type='checkbox' name='usersList[]' value='" . $forValue . "'>";
        	echo ($ID+1) . ' - ' . $NAME . ' ' . $SURNAME . ' ' . $NUMBER;
        	echo "</input>";
        	echo "<br>";
    	}
	}
    protected function deliteUsers($list){
        $identifiresUsersForDelite = [];
        foreach($list as $user){
            $parse = explode('-', $user);
            $identifiresUsersForDelite[] = $parse[0]-1;
        }

        foreach(($this->getListOfFiles()) as $file_name){
          	$idInFile = explode('_', explode('/', $file_name)[1])[0];
            foreach($identifiresUsersForDelite as $idInList){
                if($idInList == $idInFile){
                    
                    unlink($file_name);
                }
            }
        }
        echo "<link rel='stylesheet' href='main.css'>Выбранные пользователи успешно удалены";
        
    }

}

class Registration extends Kernel{
    public function __construct($name, $surname, $email, $number, $confer, $pay){
        $this->data =[$name, $surname, $email, $number, $confer, $pay]; //Упаковать данные от пользователя
        $this->createUser($this->data);
    }
}

?>